package goose_game

import org.junit.Test

import org.junit.Assert.*
import kotlin.random.Random
import kotlin.random.nextInt

class GooseGameCalculatorTest {

    @Test
    fun calculateResultPosition6() {
        testPosition(6, "The Bridge: Go to space 12")
    }

    @Test
    fun calculateResultPosition12() {
        testPosition(12, "Move two spaces forward.")
    }

    @Test
    fun calculateResultPosition17() {
        testPosition(17, "Stay in space 17")
    }

    @Test
    fun calculateResultPosition19() {
        testPosition(19, "The Hotel: Stay for (miss) one turn")
    }

    @Test
    fun calculateResultPosition31() {
        testPosition(
            31,
            "The Well: Wait until someone comes to pull you out - they then take your place"
        )
    }

    @Test
    fun calculateResultPosition42() {
        testPosition(42, "The Maze: Go back to space 39")
    }

    @Test
    fun calculateResultPositions50to55() {
        val expected = "The Prison: Wait until someone comes to release you - they then take your place"
        for (position in 50..55) {
            testPosition(position, expected)
        }
    }

    @Test
    fun calculateResultPosition58() {
        val expected = "Death: Return your piece to the beginning - start the game again"
        testPosition(58, expected)
    }

    @Test
    fun calculateResult63Position() {
        testPosition(63, "Finish: you ended the game")
    }

    @Test
    fun calculateResultPositionGreaterThan63() {
        val position = Random(100).nextInt(64..100)
        testPosition(position, "Move to space 53 and stay in prison for two turns")
    }

    private fun testPosition(position: Int, expected: String) {
        val calculator = GooseGameCalculatorFactory.create()
        val actual = calculator.calculateMessage(position)
        assertEquals(expected, actual)
    }
}