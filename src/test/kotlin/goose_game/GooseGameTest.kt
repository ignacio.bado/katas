package goose_game

import org.junit.Test

import org.junit.Assert.*

class GooseGameTest {

    @Test
    fun playTest() {
        val output = mutableListOf<String>()
        val printer = Printer { out -> output.add(out) }
        val calculator = GooseGameCalculator { "Some statement" }

        GooseGame(printer, calculator).play()

        assertEquals(List(63) { "Some statement" }, output)
    }
}