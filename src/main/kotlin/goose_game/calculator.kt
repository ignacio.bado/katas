package goose_game

fun interface GooseGameCalculator {
    fun calculateMessage(position: Int): String
}

class GooseGameCalculatorImpl(private vararg val rules: GooseGameRule) : GooseGameCalculator {
    override fun calculateMessage(position: Int): String {
        rules.forEach { rule ->
            if (rule.applies(position)) return rule.calculateMessage(position)
        }
        return "Stay in space $position"
    }
}
