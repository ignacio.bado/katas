package goose_game

import java.lang.RuntimeException

interface GooseGameRule : GooseGameCalculator {
    fun applies(position: Int): Boolean
}
class SinglePositionRule : GooseGameRule {
    private val positions = mapOf(
        6 to "The Bridge: Go to space 12",
        19 to "The Hotel: Stay for (miss) one turn",
        31 to "The Well: Wait until someone comes to pull you out - they then take your place",
        42 to "The Maze: Go back to space 39",
        58 to "Death: Return your piece to the beginning - start the game again",
        63 to "Finish: you ended the game"
    )

    override fun applies(position: Int) = positions.contains(position)

    override fun calculateMessage(position: Int): String {
        return positions[position] ?: throw RuntimeException("Position not found")
    }
}

class PrisonRule : GooseGameRule {
    override fun applies(position: Int) = position in 50..55

    override fun calculateMessage(position: Int): String {
        return "The Prison: Wait until someone comes to release you - they then take your place"
    }
}

class OverflowRule : GooseGameRule {
    override fun applies(position: Int) = position > 63

    override fun calculateMessage(position: Int) = "Move to space 53 and stay in prison for two turns"
}

class MultipleOf6Rule : GooseGameRule {
    override fun applies(position: Int) = position % 6 == 0

    override fun calculateMessage(position: Int) = "Move two spaces forward."
}