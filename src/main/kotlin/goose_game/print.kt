package goose_game

fun interface Printer {
    fun print(out: String)
}

class StandardOutputPrinter : Printer {
    override fun print(out: String) {
        println(out)
    }
}