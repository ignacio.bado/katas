package goose_game

fun main() {
    GooseGame().play()
}

class GooseGame(
    private val printer: Printer = StandardOutputPrinter(),
    private val calculator: GooseGameCalculator = GooseGameCalculatorFactory.create()
) {
    fun play() {
        for(position in 1..63) {
            printer.print(calculator.calculateMessage(position))
        }
    }
}

object GooseGameCalculatorFactory {
    fun create(): GooseGameCalculator = GooseGameCalculatorImpl(
        SinglePositionRule(),
        PrisonRule(),
        OverflowRule(),
        MultipleOf6Rule()
    )
}